#!/bin/sh
set -ex

aws cloudformation deploy \
--template-file conf/aws/cfn/s3/s3.yml \
--stack-name $ENVIRONMENT-devopsgo-s3 \
--parameter-overrides StageParameter=$ENVIRONMENT RegionParameter=$AWS_REGION AWSAccountParameter=$AWS_ACCOUNT_ID \
--tags project=$PROJECT_NAME squad=$SQUAD_NAME \
--region $AWS_REGION \
--no-fail-on-empty-changeset

aws cloudformation deploy \
--template-file conf/aws/cfn/sns/sns.yml \
--stack-name $ENVIRONMENT-devopsgo-sns \
--parameter-overrides StageParameter=$ENVIRONMENT RegionParameter=$AWS_REGION AWSAccountParameter=$AWS_ACCOUNT_ID\
--tags project=$PROJECT_NAME squad=$SQUAD_NAME \
--capabilities CAPABILITY_NAMED_IAM \
--region $AWS_REGION \
--no-fail-on-empty-changeset

aws cloudformation deploy \
--template-file conf/aws/cfn/sqs/sqs.yml \
--stack-name $ENVIRONMENT-devopsgo-sqs \
--parameter-overrides StageParameter=$ENVIRONMENT RegionParameter=$AWS_REGION AWSAccountParameter=$AWS_ACCOUNT_ID \
--tags project=$PROJECT_NAME squad=$SQUAD_NAME \
--capabilities CAPABILITY_NAMED_IAM \
--region $AWS_REGION \
--no-fail-on-empty-changeset

aws cloudformation deploy \
--template-file conf/aws/cfn/sqs/sqs-subscription.yml \
--stack-name $ENVIRONMENT-devopsgo-sns-subscription \
--parameter-overrides StageParameter=$ENVIRONMENT RegionParameter=$AWS_REGION AWSAccountParameter=$AWS_ACCOUNT_ID \
--tags project=$PROJECT_NAME squad=$SQUAD_NAME \
--capabilities CAPABILITY_NAMED_IAM \
--region $AWS_REGION \
--no-fail-on-empty-changeset

aws cloudformation deploy \
--template-file conf/aws/cfn/sqs/sqs-policy.yml \
--stack-name $ENVIRONMENT-devopsgo-sqs-policy \
--parameter-overrides StageParameter=$ENVIRONMENT RegionParameter=$AWS_REGION AWSAccountParameter=$AWS_ACCOUNT_ID \
--tags project=$PROJECT_NAME squad=$SQUAD_NAME \
--capabilities CAPABILITY_NAMED_IAM \
--region $AWS_REGION \
--no-fail-on-empty-changeset

envsubst < conf/aws/cfn/sns/template/sns-add-permission-s3-publish.template > conf/aws/cfn/sns/sns-add-permission-s3-publish.json
aws sns set-topic-attributes \
--region $AWS_REGION \
--topic-arn "arn:aws:sns:us-east-1:$AWS_ACCOUNT_ID:$ENVIRONMENT-devopsgo" \
--attribute-name Policy \
--attribute-value file://conf/aws/cfn/sns/sns-add-permission-s3-publish.json

envsubst < conf/aws/cfn/s3/template/s3-devopsgo-images-notification.template > conf/aws/cfn/s3/s3-devopsgo-images-notification.json
aws s3api put-bucket-notification-configuration \
--region $AWS_REGION \
--bucket $ENVIRONMENT-devopsgo-images-$AWS_REGION-$AWS_ACCOUNT_ID \
--notification-configuration file://conf/aws/cfn/s3/s3-devopsgo-images-notification.json